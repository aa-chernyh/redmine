# Настройки для запуска redmine

### Для запуска на debian-based системе необходимо выполнить следующие шаги:

Установить необходимый софт:
```text
sudo apt update
sudo apt upgrade

sudo apt install nano
sudo apt install git
sudo apt install zip

sudo apt install docker
sudo apt install docker-compose

sudo groupadd docker
sudo usermod -aG docker $USER
newgrp docker
```

Активировать сервисы docker-а:
```text
sudo systemctl enable docker.service
sudo systemctl enable containerd.service
```

Клонировать проект:
```text
cd /var/www/ && git clone https://gitlab.com/aa-chernyh/redmine.git
```

Скопировать systemctl service Redmine-а и активировать его:
```text
sudo cp -r /var/www/redmine/services/* /etc/systemd/system/
sudo systemctl daemon-reload
sudo systemctl enable redmine
```

### Доступ

Redmine теперь доступен по адресу localhost:8080.
<br>
Отредактировать порт можно в файле /docker/docker-compose.yml.